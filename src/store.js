import { createStore, applyMiddleware, compose } from "redux";
import reducers from "./reducers";
import ReduxThunk from "redux-thunk";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["users"]
};

const persistedReducer = persistReducer(persistConfig, reducers);

export default createStore(persistedReducer, applyMiddleware(ReduxThunk));

// const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
// export default store;

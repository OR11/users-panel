import React, { Component } from "react";
import { Container, FormGroup, Label, Button, Alert } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { logInSchema } from "../utils/ValidationSchemas";
import { Formik, Field, Form } from "formik";
import { TextInput } from "../components/common";
import PropTypes from "prop-types";

class LoginForm extends Component {
  static propTypes = {
    login: PropTypes.func,
    error: PropTypes.string
  };
  render() {
    const { login, error } = this.props;

    return (
      <Container>
        <h2>Sign In</h2>

        <Formik
          initialValues={{
            email: "",
            password: ""
          }}
          validationSchema={logInSchema}
          onSubmit={values => {
            login(values);
          }}
        >
          <Container>
            <Form>
              <FormGroup>
                <Label>
                  <FontAwesomeIcon icon="envelope" />
                  Email
                </Label>
                <Field name="email" type={"email"} component={TextInput} />
              </FormGroup>

              <FormGroup>
                <Label>
                  <FontAwesomeIcon icon="key" />
                  Password
                </Label>
                <Field
                  name="password"
                  type={"password"}
                  component={TextInput}
                />
              </FormGroup>
              <FormGroup>
                {error && <Alert color="danger">{error}</Alert>}
              </FormGroup>

              <Button type="submit">Submit</Button>
            </Form>
          </Container>
        </Formik>
      </Container>
    );
  }
}

export default LoginForm;

import React, { Component } from "react";
import { Table, Container } from "reactstrap";
import { SimpleModal, UserModal } from "./common";
import { TableRow } from "./common";
import PropTypes from "prop-types";

class UsersList extends Component {
  static propTypes = {
    users: PropTypes.array,
    rmUser: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      currentUser: {}
    };
    this.modal = null;
    this.usrModal = null;
  }

  handleDelete = (e, currentUser) => {
    e.preventDefault();

    this.setState({ currentUser });

    this.modal.toggle();
  };

  showUser = (e, currentUser) => {
    e.preventDefault();
    this.setState({ currentUser });

    this.usrModal.toggle();
  };

  renderRows = () => {
    const { users = [] } = this.props;

    return typeof users === "object"
      ? users.map((usr, i) => {
          return (
            <TableRow
              key={i}
              usr={usr}
              index={i}
              handleDelete={this.handleDelete}
              showUser={this.showUser}
            />
          );
        })
      : null;
  };

  render() {
    const { currentUser } = this.state;
    const { rmUser } = this.props;
    return (
      <Container>
        <Table hover>
          <thead>
            <tr>
              <th>#</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Edit</th>
              <th>Delete</th>
              <th>Details</th>
            </tr>
          </thead>
          <tbody>{this.renderRows()}</tbody>
        </Table>
        <SimpleModal
          ref={modal => (this.modal = modal)}
          user={currentUser}
          rmUser={rmUser}
        />
        <UserModal ref={modal => (this.usrModal = modal)} user={currentUser} />
      </Container>
    );
  }
}

export default UsersList;

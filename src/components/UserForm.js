import React, { Component } from "react";
import { userSchema } from "../utils/ValidationSchemas";
import { Formik, Field, Form } from "formik";
import { FormGroup, Label, Button, Container } from "reactstrap";
import { TextInput } from "../components/common";
import PropTypes from "prop-types";

class UserForm extends Component {
  static propTypes = {
    save: PropTypes.func,
    user: PropTypes.shape({
      job: PropTypes.string,
      name: PropTypes.string,
      last_name: PropTypes.string,
      first_name: PropTypes.string
    }),
    error: PropTypes.string
  };
  render() {
    const { save, user } = this.props;

    return (
      <Formik
        enableReinitialize={true}
        initialValues={user}
        validationSchema={userSchema}
        onSubmit={values => {
          save(values);
        }}
      >
        <Container>
          <Form>
            <FormGroup>
              <Label for="exampleName">Name</Label>
              <Field name="name" type={"text"} component={TextInput} />
            </FormGroup>
            <FormGroup>
              <Label for="exampleEmail">Job</Label>
              <Field name="job" type={"text"} component={TextInput} />
            </FormGroup>
            <FormGroup>
              <Label for="first_name">First Name</Label>
              <Field name="first_name" type={"text"} component={TextInput} />
            </FormGroup>
            <FormGroup>
              <Label for="last_name">Last Name</Label>
              <Field name="last_name" type={"text"} component={TextInput} />
            </FormGroup>
            {/* <FormGroup>
              {error && <Alert color="danger">{error}</Alert>}
            </FormGroup> */}
            <Button>Submit</Button>
          </Form>
        </Container>
      </Formik>
    );
  }
}

export default UserForm;

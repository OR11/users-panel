import React, { Component } from "react";
import { Container, Alert, FormGroup, Label, Button } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { RegisterSchema } from "../utils/ValidationSchemas";
import { Formik, Field, Form } from "formik";
import { TextInput } from "../components/common";
import PropTypes from "prop-types";

class RegisterForm extends Component {
  static propTypes = {
    register: PropTypes.func,
    error: PropTypes.string
  };

  render() {
    const { register, error } = this.props;
    return (
      <Container>
        <h2>Register</h2>

        <Formik
          initialValues={{
            email: "",
            password: "",
            address: "",
            name: ""
          }}
          validationSchema={RegisterSchema}
          onSubmit={values => {
            register(values);
          }}
        >
          <Container>
            <Form>
              <FormGroup>
                <Label>
                  <FontAwesomeIcon icon="envelope" />
                  Email
                </Label>
                <Field name="email" type={"email"} component={TextInput} />
              </FormGroup>

              <FormGroup>
                <Label>
                  <FontAwesomeIcon icon="key" />
                  Password
                </Label>
                <Field
                  name="password"
                  type={"password"}
                  component={TextInput}
                />
              </FormGroup>

              <FormGroup>
                <Label>
                  <FontAwesomeIcon icon="map-marked-alt" />
                  Address
                </Label>
                <Field name="address" type={"text"} component={TextInput} />
              </FormGroup>

              <FormGroup>
                <Label>
                  <FontAwesomeIcon icon="address-card" />
                  Name
                </Label>
                <Field name="name" type={"text"} component={TextInput} />
              </FormGroup>
              <FormGroup>
                {error && <Alert color="danger">{error}</Alert>}
              </FormGroup>

              <Button type="submit">Submit</Button>
            </Form>
          </Container>
        </Formik>
      </Container>
    );
  }
}

export default RegisterForm;

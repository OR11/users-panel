import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

export default ChildComponent => {
  class ComposedComponent extends Component {
    static propTypes = {
      auth: PropTypes.bool
    };
    // Our component just got rendered
    componentDidMount() {
      this.shouldNavigateAway();
    }

    // Our component just got updated
    componentDidUpdate() {
      this.shouldNavigateAway();
    }

    shouldNavigateAway() {
      const token = localStorage.getItem("token");
      if (this.props.auth && token) {
        this.props.history.push("/");
      }
    }

    render() {
      return <ChildComponent {...this.props} />;
    }
  }

  const mapStateToProps = state => {
    return { auth: state.auth.isAuthenticated };
  };

  return connect(mapStateToProps)(ComposedComponent);
};

/* eslint-disable no-unused-vars */
import React, { Component, Fragment } from "react";

import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Collapse,
  NavbarToggler
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { Link } from "react-router-dom";

class Header extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    const { auth, hadleLogOut } = this.props;

    return (
      <div>
        <Navbar color="dark" light expand="md">
          <NavbarBrand className="head-link" tag={Link} to="/">
            Home
          </NavbarBrand>{" "}
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              {auth && (
                <Fragment>
                  <NavItem>
                    <NavLink className="head-link" tag={Link} to="/addUser">
                      <FontAwesomeIcon icon="plus-circle" size="1x" />
                      Add User
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className="head-link"
                      tag={Link}
                      to="/login"
                      onClick={hadleLogOut}
                    >
                      <FontAwesomeIcon icon="sign-out-alt" size="1x" />
                      Logout
                    </NavLink>
                  </NavItem>
                </Fragment>
              )}

              {!auth && (
                <Fragment>
                  <NavItem>
                    <NavLink className="head-link" tag={Link} to="/login">
                      <FontAwesomeIcon icon="sign-out-alt" size="1x" />
                      Login
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink className="head-link" tag={Link} to="/register">
                      <FontAwesomeIcon icon="user-plus" size="1x" />
                      Register
                    </NavLink>
                  </NavItem>
                </Fragment>
              )}
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

export { Header };

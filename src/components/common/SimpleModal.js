import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import PropTypes from "prop-types";

class SimpleModal extends React.Component {
  static propTypes = {
    rmUser: PropTypes.func,
    user: PropTypes.shape({
      id: PropTypes.number,
      last_name: PropTypes.string,
      first_name: PropTypes.string
    })
  };
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };
  }
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  deleteUsr = id => {
    const { rmUser } = this.props;
    rmUser(id);
    this.toggle();
  };

  render() {
    const {
      user: { first_name, last_name, id }
    } = this.props;
    return (
      <Modal
        isOpen={this.state.modal}
        toggle={this.toggle}
        onClosed={this.state.closeAll ? this.toggle : undefined}
      >
        <ModalHeader>Delete User</ModalHeader>
        <ModalBody>
          Are U Sure to Delete {`${first_name} ${last_name}`}
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={() => this.deleteUsr(id)}>
            Delete
          </Button>{" "}
          <Button color="secondary" onClick={this.toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export { SimpleModal };

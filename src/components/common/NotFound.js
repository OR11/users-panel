import React from "react";
import { Link } from "react-router-dom";
import PageNotFound from "../../assets/error-page.png";
import { Button } from "reactstrap";

const NotFound = () => (
  <div>
    <img src={PageNotFound} alt={`404 Not Found`} />
    <center>
      <Link to="/">
        <Button color="primary">Return to Home Page</Button>
      </Link>
    </center>
  </div>
);

export default NotFound;

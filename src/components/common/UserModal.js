import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

class UserModal extends React.Component {
  static propTypes = {
    user: PropTypes.shape({
      id: PropTypes.number,
      avatar: PropTypes.string,
      last_name: PropTypes.string,
      first_name: PropTypes.string
    })
  };

  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };
  }
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  render() {
    const {
      user: { id, first_name = "", last_name = "", avatar = "" }
    } = this.props;
    return (
      <Modal
        isOpen={this.state.modal}
        toggle={this.toggle}
        onClosed={this.state.closeAll ? this.toggle : undefined}
      >
        <ModalHeader>User Details</ModalHeader>
        <ModalBody>
          <div className="row">
            <div className="col-md-9 d-flex align-items-center justify-content-around">
              <div>
                <label className="">First Name : </label>
                <div>{first_name}</div>
              </div>
              <div>
                <label>Last Name :</label>
                <div>{last_name}</div>
              </div>
            </div>

            <div className="col-md-3">
              <img
                src={avatar}
                className="rounded-circle"
                alt="Smiley face"
                height="100"
                width="100"
              />
            </div>
          </div>
        </ModalBody>
        <ModalFooter>
          <Link to={`/editUser/${id}`}>
            <Button color="danger" onClick={this.toggle}>
              Edit
            </Button>{" "}
          </Link>
          <Button color="secondary" onClick={this.toggle}>
            Close
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export { UserModal };

import React from "react";
import { Label } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const TableRow = ({ usr, handleDelete, showUser, index }) => {
  const { first_name, last_name, id } = usr;

  return (
    <tr>
      <th scope="row">{index + 1}</th>
      <td>{first_name}</td>
      <td>{last_name}</td>
      <td>
        <Link to={`/editUser/${id}`}>
          <Label>
            <FontAwesomeIcon icon="edit" />
          </Label>
        </Link>
      </td>
      <td>
        <Label onClick={e => handleDelete(e, usr)}>
          <FontAwesomeIcon icon="trash" />
        </Label>
      </td>

      <td>
        <Label onClick={e => showUser(e, usr)}>
          <FontAwesomeIcon icon="info-circle" />
        </Label>
      </td>
    </tr>
  );
};

TableRow.propTypes = {
  handleDelete: PropTypes.func,
  showUser: PropTypes.func,
  index: PropTypes.number,
  usr: PropTypes.shape({
    id: PropTypes.number,
    avatar: PropTypes.string,
    last_name: PropTypes.string,
    first_name: PropTypes.string
  })
};
export { TableRow };

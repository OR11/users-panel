import React from "react";

class EmptyData extends React.Component {
  render() {
    const { image, text } = this.props;
    return (
      <div className="empty">
        <div className="content">
          <img src={image} alt="No Data" />
          <h6>{text}</h6>
        </div>

        <style jsx>{`
          .empty {
            width: 100%;
            max-width: 100%;
            max-height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
          }
          .empty .content {
            background-color: #fff;
            border-radius: 5px;
            text-align: center;
            display: inline-block;
            padding: 20px;
            margin: 10px;
          }
          .empty img {
            width: 500px;
          }
        `}</style>
      </div>
    );
  }
}

export { EmptyData };

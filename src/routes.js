import React from "react";
import { Route, Switch } from "react-router-dom";
import loginPage from "./containers/loginPage";
import RegisterPage from "./containers/RegisterPage";
import UsersPage from "./containers/UsersPage";
import UserFormPage from "./containers/UserFormPage";
import RequireAuth from "./components/common/RequireAuth";
import NotFound from "./components/common/NotFound";
import RequireNotAuth from "./components/common/RequireNotAuth";

const Routes = () => (
  <Switch>
    <Route exact path="/" component={RequireAuth(UsersPage)} />
    <Route path={"/login"} component={RequireNotAuth(loginPage)} />
    <Route path={"/register"} component={RequireNotAuth(RegisterPage)} />
    <Route path={"/editUser/:id"} component={RequireAuth(UserFormPage)} />
    <Route path={"/addUser"} component={RequireAuth(UserFormPage)} />
    <Route path="*" exact component={NotFound} />
  </Switch>
);

export default Routes;

import request from './ApiCentral';

export function createUser(body) {
  return request({
    url: 'users',
    method: 'POST',
    data: body
  });
}
export function fetchUsers(p = 1) {
  return request({
    url: `users?page=${p}`,
    method: 'GET'
  });
}

export function deleteUser(id) {
  return request({
    url: `users/${id}`,
    method: 'DELETE'
  });
}

export function getSingleUser(id) {
  return request({
    url: `users/${id}`,
    method: 'GET'
  });
}

export function updateUser(body) {
  return request({
    url: `users/${body.id}`,
    method: 'PUT',
    data: body
  });
}

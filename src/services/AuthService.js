import request from "./ApiCentral";

export function login(body) {
  return request({
    url: "login",
    method: "POST",
    data: body
  });
}

export function register(body) {
  return request({
    url: "register",
    method: "POST",
    data: body
  });
}

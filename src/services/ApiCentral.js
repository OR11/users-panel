import axios from 'axios';
import { BASE_URL } from '../utils/config';

const request = async function(options, isHeader = true) {
  let authHeader = null;
  if (isHeader) {
    let token = await localStorage.getItem('token');

    if (token) {
      authHeader = token;
    }
  }

  const client = axios.create({
    baseURL: BASE_URL,
    headers: {
      Authorization: authHeader
    }
  });

  const onSuccess = function(response) {
    return response.data;
  };

  const onError = function(error) {
    return Promise.reject(error.response || error.message);
  };

  return client(options)
    .then(onSuccess)
    .catch(onError);
};

export default request;

import React, { Component } from "react";
import RegisterForm from "../components/RegisterForm";
import { connect } from "react-redux";
import { signUp } from "../actions";
import { Loader } from "../components/common";

class RegisterPage extends Component {
  register = body => {
    const { signUp } = this.props;

    signUp(body, this.goToHome);
  };

  goToHome = () => {
    this.props.history.push("/");
  };

  render() {
    const { loading, error } = this.props;
    return (
      <div>
        <Loader
          loading={loading}
          message={`Please Wait ....Creating your new Account`}
        />
        <RegisterForm register={this.register} error={error} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { loading, error } = state.auth.register;

  return {
    loading,
    error
  };
};

export default connect(
  mapStateToProps,
  {
    signUp
  }
)(RegisterPage);

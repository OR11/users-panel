import React, { Component } from "react";
import UsersList from "../components/UsersList";
import { connect } from "react-redux";
import * as actions from "../actions";
import { Loader } from "../components/common";
import { isAuthenticated } from "../utils/auth";
import { EmptyData } from "../components/common";
import empty from "../assets/empty.png";

class UsersPage extends Component {
  componentDidMount() {
    const { getUsers, data, authenticateUser } = this.props;

    if (isAuthenticated()) {
      authenticateUser();
    }

    //prevent from send request to fake add or edit
    //because https://reqres.in  doesnt save data
    if (!data.length) {
      getUsers();
    }
  }

  handleDeleteUser = id => {
    this.props.rmUser(id);
  };

  render() {
    const { data, loading } = this.props;
    return loading ? (
      <Loader
        loading={loading}
        message={`Please Wait .... getting list of users`}
      />
    ) : (
      <div>
        <UsersList users={data} rmUser={this.handleDeleteUser} />

        {!data.length && (
          <EmptyData image={empty} text={"There Is no Users Here"} />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { loading, data } = state.users;

  return {
    loading,
    data
  };
};

export default connect(
  mapStateToProps,
  actions
)(UsersPage);

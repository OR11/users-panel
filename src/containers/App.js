/* eslint-disable no-unused-vars */
import React, { Component, Fragment } from "react";
import { BrowserRouter } from "react-router-dom";
import Routes from "../routes";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faEnvelope,
  faKey,
  faEdit,
  faTrash,
  faInfoCircle,
  faInfo,
  faMapMarkedAlt,
  faAddressCard,
  faPlusCircle,
  faSignOutAlt,
  faUserPlus
} from "@fortawesome/free-solid-svg-icons";
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Container,
  Collapse,
  NavbarToggler
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { Link } from "react-router-dom";
import "../app.css";
import * as actions from "../actions";
import { connect } from "react-redux";
import { Header } from "../components/common";

library.add(
  faEnvelope,
  faKey,
  faEdit,
  faTrash,
  faInfoCircle,
  faInfo,
  faMapMarkedAlt,
  faAddressCard,
  faPlusCircle,
  faSignOutAlt,
  faUserPlus
);

class App extends Component {
  componentDidMount() {
    const { setAuthenticateFlag } = this.props;
    const token = localStorage.getItem("token");
    if (token) {
      setAuthenticateFlag();
    }
  }

  hadleLogOut = () => {
    this.props.signOut(this.goToLogIn);
  };
  render() {
    const { auth } = this.props;
    return (
      <BrowserRouter>
        <Container>
          <Header auth={auth} hadleLogOut={this.hadleLogOut} />
          <Routes />
        </Container>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => {
  return { auth: state.auth.isAuthenticated };
};

export default connect(
  mapStateToProps,
  actions
)(App);

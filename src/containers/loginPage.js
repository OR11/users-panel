import React, { Component } from "react";
import LoginForm from "../components/loginForm";
import { connect } from "react-redux";
import { singIn } from "../actions";
import { Loader } from "../components/common";

class loginPage extends Component {
  login = body => {
    const { singIn } = this.props;

    singIn(body, this.goToHome);
  };

  goToHome = () => {
    this.props.history.push("/");
  };

  render() {
    const { loading, error } = this.props;
    return (
      <div>
        <Loader
          loading={loading}
          message={`Please Wait ....While  Logging in`}
        />

        <LoginForm login={this.login} error={error} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { loading, error } = state.auth.login;

  return {
    loading,
    error
  };
};

export default connect(
  mapStateToProps,
  {
    singIn
  }
)(loginPage);

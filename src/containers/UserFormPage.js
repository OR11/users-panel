import React, { Component } from "react";
import UserForm from "../components/UserForm";
import { connect } from "react-redux";
import { Loader } from "../components/common";
import * as actions from "../actions";
import { EmptyData } from "../components/common";
import noUser from "../assets/notHere.png";
import { Button } from "reactstrap";
import { Link } from "react-router-dom";

class UserFormPage extends Component {
  state = {
    isNew: false
  };

  componentDidMount() {
    this.init();
  }

  get getId() {
    return parseInt(this.props.match.params.id, 10);
  }

  save = data => {
    const { isNew } = this.state,
      { addUser, editUser } = this.props;

    isNew ? addUser(data, this.goBack) : editUser(data, this.goBack);
  };

  init = () => {
    let id = this.getId;

    if (id) {
      // this.fetchUser(id);
      this.fetchUser(this.fetchUserFromStore(id));
    } else {
      this.setState(() => {
        return { isNew: true };
      });
    }
  };

  fetchUser = user => {
    const { getUser } = this.props;

    getUser(user);
  };

  goBack = () => {
    this.props.history.goBack();
  };

  fetchUserFromStore = id => {
    const { usersList } = this.props;

    // make it == to enable coherence
    //because https://reqres.in genreate string id when create new user
    //and return id type numbers when get list of users

    return usersList.find(x => x.id == id);
  };

  componentWillUnmount() {
    const { clearForm } = this.props;
    clearForm();
  }
  render() {
    const { user, loading, error, isFetched } = this.props;
    const { isNew } = this.state;
    return loading ? (
      <Loader
        loading={loading}
        message={`Please Wait .... Operation on Progress`}
      />
    ) : (
      <div>
        {(isNew || isFetched) && (
          <UserForm save={this.save} user={user} error={error} />
        )}
        {!isFetched && !isNew && (
          <div>
            <EmptyData image={noUser} text={"User Not Found"} />
            <center>
              <Link to="/addUser">
                <Button color="primary">Add New User</Button>
              </Link>
            </center>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  const {
    user: { loading, data, user, error, isFetched }
  } = state;

  return {
    loading,
    data,
    user,
    error,
    isFetched,
    usersList: state.users.data
  };
};

export default connect(
  mapStateToProps,
  actions
)(UserFormPage);

import {
  FETCH_USERS_SUCCESS,
  REMOVE_USER_SUCCESS,
  UPDATE_USER_SUCCESS,
  CREATE_USER_SUCCESS,
  START_FETCH_USERS,
  FETCH_USERS_FAILED,
  REMOVE_USER_START,
  REMOVE_USER_FAILED
} from "../actions/types";

const INITIAL_STATE = {
  data: [],
  page: 0,
  perPage: 10,
  total_pages: false,
  loading: false,
  error: ""
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_USERS_SUCCESS:
      return { ...state, data: action.payload, loading: false };

    case REMOVE_USER_FAILED:
      return { ...state, error: action.payload, loading: false };

    case START_FETCH_USERS:
      return { ...state, loading: true };

    case FETCH_USERS_FAILED:
      return { ...state, error: action.payload, loading: false };

    case REMOVE_USER_START:
      return { ...state, loading: true };

    case REMOVE_USER_SUCCESS:
      return {
        ...state,
        data: state.data.filter(a => a.id !== action.payload),
        loading: false
      };
    case CREATE_USER_SUCCESS:
      return {
        ...state,
        data: [action.payload, ...state.data],
        loading: false
      };

    case UPDATE_USER_SUCCESS:
      return {
        ...state,
        data: state.data.map(item => {
          if (item.id === action.payload.id) {
            return { ...item, ...action.payload };
          }
          return item;
        })
      };

    default:
      return state;
  }
}

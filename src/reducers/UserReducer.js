import {
  FETCH_SINGLE_USER_SUCCESS,
  FETCH_SINGLE_USER_START,
  CREATE_USER_START,
  CREATE_USER_SUCCESS,
  CREATE_USER_FAILED,
  UPDATE_USER_FAILED,
  UPDATE_USER_START,
  UPDATE_USER_SUCCESS,
  FETCH_SINGLE_USER_FAILED,
  CLEAR_FORM
} from "../actions/types";

const INITIAL_STATE = {
  data: {},
  loading: false,
  user: {
    job: "",
    name: "",
    last_name: "",
    first_name: ""
  },
  error: "",
  isFetched: false
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_SINGLE_USER_SUCCESS:
      return {
        ...state,
        user: { ...state.user, ...action.payload },
        loading: false,
        error: "",
        isFetched: true
      };
    case FETCH_SINGLE_USER_START:
      return {
        ...state,
        loading: true
      };
    case FETCH_SINGLE_USER_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
        isFetched: false
      };

    case CREATE_USER_START:
      return { ...state, loading: true };
    case CREATE_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        user: { job: "", name: "", last_name: "", first_name: "" },
        error: ""
      };
    case CREATE_USER_FAILED:
      return { ...state, error: action.payload };
    case UPDATE_USER_START:
      return { ...state, loading: true };

    case UPDATE_USER_FAILED:
      return { ...state, error: action.payload };
    case UPDATE_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        user: { job: "", name: "", last_name: "", first_name: "" },
        error: ""
      };
    case CLEAR_FORM:
      return {
        ...state,
        loading: false,
        user: { job: "", name: "", last_name: "", first_name: "" },
        error: ""
      };

    default:
      return state;
  }
}

import {
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  START_LOGIN,
  REGISTER_SUCCESS,
  START_REGISTER,
  REGISTER_FAILED,
  LOGIN_FAILED,
  SET_IS_AUTH_FLAG
} from '../actions/types';

const INITIAL_STATE = {
  login: {
    loading: false,
    error: ''
  },
  register: {
    loading: false,
    error: ''
  },
  isAuthenticated: false
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case START_LOGIN:
      return { ...state, login: { ...state.login, loading: true } };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        login: { ...state.login, loading: false, error: '' }
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        register: { ...state.register, error: '', loading: false }
      };

    case SET_IS_AUTH_FLAG:
      return {
        ...state,
        isAuthenticated: true
      };

    case LOGOUT_SUCCESS:
      return { ...state, isAuthenticated: false };

    case START_REGISTER:
      return { ...state, register: { ...state.register, loading: true } };

    case LOGIN_FAILED:
      return {
        ...state,
        login: { ...state.login, loading: false, error: action.payload }
      };

    case REGISTER_FAILED:
      return {
        ...state,
        register: { ...state.register, error: action.payload, loading: false }
      };

    default:
      return state;
  }
}

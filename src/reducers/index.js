import { combineReducers } from 'redux';
import UsersReducer from './UsersReducer';
import UserReducer from './UserReducer';
import AuthReducer from './AuthReducer';

export default combineReducers({
  users: UsersReducer,
  user: UserReducer,
  auth: AuthReducer
});

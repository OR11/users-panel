export const getJwt = () => {
  return localStorage.getItem('token');
};

export const isAuthenticated = () => !!getJwt();

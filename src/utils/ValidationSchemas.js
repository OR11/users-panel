import { string, object } from "yup";

//creating shcema to validate create User
export const userSchema = object().shape({
  first_name: string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  last_name: string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  name: string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  job: string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required")
});

//just comment .min(2,) and required to show error massage

export const logInSchema = object().shape({
  password: string()
    // .min(2, 'Too Short!')
    .max(50, "Too Long!"),
  // .required('Required')
  email: string()
    .email("Invalid email")
    .required("Required")
});

//just comment .min(2,) and required to show error massage

export const RegisterSchema = object().shape({
  password: string()
    // .min(2, "Too Short!")
    .max(50, "Too Long!"),
  // .required("Required")
  email: string()
    .email("Invalid email")
    .required("Required"),
  address: string()
    .min(10, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  name: string()
    .min(4, "Too Short!")
    .max(50, "Too Long!")
    .required("Required")
});

import {
  START_LOGIN,
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  REGISTER_SUCCESS,
  LOGOUT_SUCCESS,
  START_REGISTER,
  REGISTER_FAILED,
  SET_IS_AUTH_FLAG
} from './types';

import { login, register } from '../services';
//login
export function singIn(body, navigate) {
  return async dispatch => {
    try {
      dispatch({
        type: START_LOGIN,
        payload: null
      });

      let res = await login(body);

      dispatch({
        type: LOGIN_SUCCESS,
        payload: res
      });

      //store token & then navigate to home
      localStorage.setItem('token', res.token);
      navigate();
    } catch (e) {
      dispatch({
        type: LOGIN_FAILED,
        payload: e.data.error
      });
    }
  };
}

export function signUp(body, navigate) {
  return async dispatch => {
    try {
      dispatch({
        type: START_REGISTER,
        payload: null
      });

      let res = await register(body);

      dispatch({
        type: REGISTER_SUCCESS,
        payload: res
      });
      //store token & then navigate to home
      localStorage.setItem('token', res.token);
      navigate();
    } catch (e) {
      dispatch({
        type: REGISTER_FAILED,
        payload: e.data.error
      });
    }
  };
}

export function signOut() {
  debugger;
  try {
    localStorage.removeItem('token');
    return {
      type: LOGOUT_SUCCESS,
      payload: null
    };
  } catch (e) {
    console.log(e);
  }
}

export const authenticateUser = () => ({
  type: LOGIN_SUCCESS,
  payload: null
});

export const setAuthenticateFlag = () => ({
  type: SET_IS_AUTH_FLAG,
  payload: true
});

import {
  FETCH_USERS_SUCCESS,
  REMOVE_USER_SUCCESS,
  FETCH_SINGLE_USER_SUCCESS,
  CREATE_USER_SUCCESS,
  UPDATE_USER_SUCCESS,
  START_FETCH_USERS,
  FETCH_USERS_FAILED,
  CREATE_USER_START,
  CREATE_USER_FAILED,
  UPDATE_USER_START,
  UPDATE_USER_FAILED,
  FETCH_SINGLE_USER_START,
  REMOVE_USER_START,
  REMOVE_USER_FAILED,
  FETCH_SINGLE_USER_FAILED,
  CLEAR_FORM
} from "./types";

import {
  fetchUsers,
  deleteUser,
  getSingleUser,
  createUser,
  updateUser
} from "../services";

//fetch list of users
export function getUsers(p) {
  return async dispatch => {
    try {
      dispatch({
        type: START_FETCH_USERS,
        payload: null
      });

      let res = await fetchUsers(p);

      dispatch({
        type: FETCH_USERS_SUCCESS,
        payload: res.data
      });
    } catch (e) {
      dispatch({
        type: FETCH_USERS_FAILED,
        payload: e
      });
    }
  };
}

//delete user
export function rmUser(id) {
  return async dispatch => {
    try {
      dispatch({
        type: REMOVE_USER_START
      });

      await deleteUser(id);

      dispatch({
        type: REMOVE_USER_SUCCESS,
        payload: id
      });
    } catch (e) {
      dispatch({
        type: REMOVE_USER_FAILED,
        payload: e
      });
    }
  };
}

//get single user with specfic id

// export function getUser(id) {
//   return async dispatch => {
//     try {
//       dispatch({
//         type: FETCH_SINGLE_USER_START,
//         payload: null
//       });

//       let res = await getSingleUser(id);

//       dispatch({
//         type: FETCH_SINGLE_USER_SUCCESS,
//         payload: res.data
//       });
//     } catch (e) {
//       dispatch({
//         type: FETCH_SINGLE_USER_FAILED,
//         payload: "User Not Found"
//       });
//     }
//   };
// }

export function getUser(data) {
  return async dispatch => {
    try {
      dispatch({
        type: FETCH_SINGLE_USER_START,
        payload: null
      });

      // let res = await getSingleUser(id);
      if (!data) {
        dispatch({
          type: FETCH_SINGLE_USER_FAILED,
          payload: "User Not Found"
        });

        return;
      }

      dispatch({
        type: FETCH_SINGLE_USER_SUCCESS,
        payload: data
      });
    } catch (e) {
      dispatch({
        type: FETCH_SINGLE_USER_FAILED,
        payload: "User Not Found"
      });
    }
  };
}
//create new user

export function addUser(body, goBack) {
  return async dispatch => {
    try {
      dispatch({
        type: CREATE_USER_START,
        payload: null
      });

      let res = await createUser(body);

      dispatch({
        type: CREATE_USER_SUCCESS,
        payload: res
      });

      //navigate after  create new User
      //to fake add or edit
      //because https://reqres.in  doesnt save data
      goBack();
    } catch (e) {
      dispatch({
        type: CREATE_USER_FAILED,
        payload: e
      });
    }
  };
}

//update existing user
export function editUser(body, goBack) {
  return async dispatch => {
    try {
      dispatch({
        type: UPDATE_USER_START,
        payload: null
      });

      let res = await updateUser(body);

      dispatch({
        type: UPDATE_USER_SUCCESS,
        payload: res
      });

      //navigate after updsate user successfully
      //to fake add or edit
      //because https://reqres.in  doesnt save data
      goBack();
    } catch (e) {
      dispatch({
        type: UPDATE_USER_FAILED,
        payload: e
      });
    }
  };
}

export const clearForm = () => ({
  type: CLEAR_FORM,
  payload: true
});
